import React from "react";
import {IndexRedirect, Route} from "react-router";

import Admin from "app/layouts/Admin";
import RouteNotFound from "app/components/RouteNotFound";
import physical_device from "app/physical_device/urls";


const urls = (
    <Route path="/">
        <IndexRedirect to="admin/physical_device"/>
        <Route component={Admin} path="admin">
            <IndexRedirect to="physical_device"/>
            {physical_device}
            <Route path="*" component={RouteNotFound}/>
        </Route>
    </Route>
);

export default urls;
