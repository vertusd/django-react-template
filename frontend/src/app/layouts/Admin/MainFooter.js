import React from "react";


class Footer extends React.Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs"><strong>中国建设银行</strong></div>
                <strong>CCB: </strong>
                leishuo.zh
            </footer>
        );
    }
}

export default Footer;
/* <strong>Copyright &copy; 2018 <a href="#">Company</a>.</strong>
All rights reserved. */