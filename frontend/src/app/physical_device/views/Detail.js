import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {createSelector} from "reselect";

import actions from "app/actions/collection";
import DeleteButton from "app/components/DeleteButton";
import EditForm from "app/physical_device/components/EditForm";
import findModel from "app/components/higherOrder/findModel";
import Model from "app/physical_device/components/Model";


class Container extends React.Component {
    render() {
        return (
            <div>
                <Model {...this.props}/>
                <div className="text-center">
                    <DeleteButton
                        {...this.props}
                        permission="physical_device.delete_phydevice"
                    />
                    <EditForm {...this.props}/>
                </div>
            </div>
        );
    }
}

const selector = createSelector(
    (state) => state.physical_device,
    (collection) => {
        return {
            collection
        };
    }
);

const bindActions = (dispatch) => {
    return {actions: bindActionCreators(actions, dispatch)};
};

export default connect(selector, bindActions)(findModel(Container));
