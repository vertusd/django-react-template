import {Map, Record} from "immutable";

import isFilterActive from "app/utils/isFilterActive";
import constants from "./constants";


class ChangeSet extends Record({
    device_id: "",
    sn: "",
    device_alias: "",
    device_model: "",
    rack: "",
    position: "",
    u_position: "",
    device_height: "",
    vendor: "",
    logical_area: "",
    note: "",
    region: "",
    ip_addr: "",
    cloud_product: "",
    intranet_switchport: "",
    management_switchport: "",
    _errors: Map()
}){}


class PhyDevice extends Record({
    id: "0",
    constants,
    ChangeSet,
    device_id: "",
    sn: "",
    device_alias: "",
    device_model: "",
    rack: "",
    position: "",
    u_position: "",
    device_height: "",
    vendor: "",
    logical_area: "",
    note: "",
    region: "",
    ip_addr: "",
    cloud_product: "",
    intranet_switchport: "",
    management_switchport: ""
}) {
    appUrl() {
        return `/admin/physical_device/${this.id}`;
    }

    tabUrl(tab = "details") {
        return `${this.appUrl()}/${tab}`;
    }

    apiUrl() {
        return `${window.django.urls.physical_device}${this.id}/`;
    }

    toString() {
        return `${this.device_id}`;
    }
    toDevice_id() {
        return `${this.device_id}`;
    }
    toSN() {
        return `${this.sn}`;
    }
    toDevice_alias() {
        return `${this.device_alias}`;
    }
    toDevice_model() {
        return `${this.device_model}`;
    }
    toRack() {
        return `${this.rack}`;
    }
    toPosition() {
        return `${this.position}`;
    }
    toU_position() {
        return `${this.u_position}`;
    }
    toDevice_height() {
        return `${this.device_height}`;
    }
    toVendor() {
        return `${this.vendor}`;
    }
    toLogical_area() {
        return `${this.logical_area}`;
    }
    toNote() {
        return `${this.note}`;
    }
    toRegion() {
        return `${this.region}`;
    }
    toIP_addr() {
        return `${this.ip_addr}`;
    }
    toCloud_product() {
        return `${this.cloud_product}`;
    }
    toIntranet_switchport() {
        return `${this.intranet_switchport}`;
    }
    toManagement_switchport() {
        return `${this.management_switchport}`;
    }

}

class Collection extends Record({
    apiUrl: window.django.urls.physical_device,
    constants,
    ChangeSet,
    isLoading: false,
    Model: PhyDevice,
    models: Map(),
    pagination: Map({
        end_index: 0,
        page: 0,
        start_index: 0,
        total_pages: 0
    }),
    query: Map({
        page: 1,
        search: ""
    }),
    routeId: "physical_device",
    title: "PhyDevice",
    titleSingular: "PhyDevice"
}){
    appUrl() {
        return "/admin/physical_device";
    }

    isFilterActive() {
        return isFilterActive(this.query);
    }
}

export {
    Collection,
    PhyDevice
};
