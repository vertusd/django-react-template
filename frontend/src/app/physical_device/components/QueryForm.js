import React from "react";
import {Input} from "react-bootstrap";

import query from "app/components/higherOrder/query";


class Form extends React.Component {
    render() {
        const {handleChange, handleSubmit, query} = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Input
                    autoComplete="off"
                    autoFocus={true}
                    label="Device ID"
                    onChange={handleChange}
                    name="device_id"
                    type="text"
                    value={query.get("device_id")}
                />
                <Input
                    autoComplete="off"
                    label="SN"
                    onChange={handleChange}
                    name="sn"
                    type="text"
                    value={query.get("sn")}
                />
                <Input
                    autoComplete="off"
                    label="Device Alias"
                    onChange={handleChange}
                    name="device_alias"
                    type="text"
                    value={query.get("device_alias")}
                />
                <Input
                    autoComplete="off"
                    label="Device Model"
                    onChange={handleChange}
                    name="device_model"
                    type="text"
                    value={query.get("device_model")}
                />
                <input type="submit" className="hidden"/>
            </form>
        );
    }
}

export default query(Form);
