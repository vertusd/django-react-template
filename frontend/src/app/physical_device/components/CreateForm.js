import React from "react";
import {Input} from "react-bootstrap";

import create from "app/components/higherOrder/create";
import hasPermission from "app/components/higherOrder/hasPermission";


class Form extends React.Component {
    render() {
        const {changeSet, handleChange, handleSubmit} = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Input
                    bsStyle={changeSet._errors.device_id ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.device_id}
                    label="Device ID"
                    name="device_id"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.device_id}
                    //help={changeSet._errors.device_id}
                />
                <Input
                    bsStyle={changeSet._errors.sn ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.sn}
                    label="SN"
                    name="sn"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.sn}
                />
                <Input
                    bsStyle={changeSet._errors.device_alias ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.device_alias}
                    label="Device Alias"
                    name="device_alias"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.device_alias}
                />
                <Input
                    bsStyle={changeSet._errors.device_model ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.device_model}
                    label="Device Model"
                    name="device_model"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.device_model}
                />
                <Input
                    bsStyle={changeSet._errors.rack ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.rack}
                    label="Rack"
                    name="rack"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.rack}
                />
                <Input
                    bsStyle={changeSet._errors.position ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.position}
                    label="Position"
                    name="position"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.position}
                />
                <Input
                    bsStyle={changeSet._errors.u_position ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.u_position}
                    label="U Position"
                    name="u_position"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.u_position}
                />
                <Input
                    bsStyle={changeSet._errors.device_height ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.device_height}
                    label="Device Height"
                    name="device_height"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.device_height}
                />
                <Input
                    bsStyle={changeSet._errors.vendor ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.vendor}
                    label="Vendor"
                    name="vendor"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.vendor}
                />
                <Input
                    bsStyle={changeSet._errors.logical_area ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.logical_area}
                    label="Logical Area"
                    name="logical_area"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.logical_area}
                />
                <Input
                    bsStyle={changeSet._errors.note ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.note}
                    label="Note"
                    name="note"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.note}
                />
                <Input
                    bsStyle={changeSet._errors.region ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.region}
                    label="Region"
                    name="region"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.region}
                />
                <Input
                    bsStyle={changeSet._errors.ip_addr ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.ip_addr}
                    label="IP Address"
                    name="ip_addr"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.ip_addr}
                />
                <Input
                    bsStyle={changeSet._errors.cloud_product ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.cloud_product}
                    label="Cloud Product"
                    name="cloud_product"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.cloud_product}
                />
                <Input
                    bsStyle={changeSet._errors.intranet_switchport ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.intranet_switchport}
                    label="Intranet Switch Port"
                    name="intranet_switchport"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.intranet_switchport}
                />
                <Input
                    bsStyle={changeSet._errors.management_switchport ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.management_switchport}
                    label="Management Switch Port"
                    name="management_switchport"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.management_switchport}
                />

                <button type="submit" className="hidden"/>
            </form>
        );
    }
}


export default hasPermission(create(Form), "device_physical.add_phydevice");
//export default hasPermission(create(Form), "users.add_emailuser");