import React from "react";
import moment from "moment";


class Model extends React.Component {
    render() {
        const {model} = this.props;

        return (
            <dl className="dl-horizontal">
                <dt className="text-muted">Device ID</dt>
                <dd>{model.device_id}</dd>

                <dt className="text-muted">SN</dt>
                <dd>{model.sn}</dd>

                <dt className="text-muted">Device Alias</dt>
                <dd>{model.device_alias}</dd>

                <dt className="text-muted">Device Model</dt>
                <dd>{model.device_model}</dd>

                <dt className="text-muted">Rack</dt>
                <dd>{model.rack}</dd>

                <dt className="text-muted">Position</dt>
                <dd>{model.position}</dd>

                <dt className="text-muted">U Position</dt>
                <dd>{model.u_position}</dd>

                <dt className="text-muted">Device Height</dt>
                <dd>{model.device_height}</dd>

                <dt className="text-muted">Vendor</dt>
                <dd>{model.vendor}</dd>

                <dt className="text-muted">Logical Area</dt>
                <dd>{model.logical_area}</dd>

                <dt className="text-muted">Note</dt>
                <dd>{model.note}</dd>

                <dt className="text-muted">Region</dt>
                <dd>{model.region}</dd>

                <dt className="text-muted">IP Address</dt>
                <dd>{model.ip_addr}</dd>

                <dt className="text-muted">Cloud Product</dt>
                <dd>{model.cloud_product}</dd>

                <dt className="text-muted">Intranet Switch Port</dt>
                <dd>{model.intranet_switchport}</dd>

                <dt className="text-muted">Management Switch Port</dt>
                <dd>{model.management_switchport}</dd>

            </dl>
        );
    }
}

export default Model;
