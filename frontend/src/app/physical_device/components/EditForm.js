import React from "react";
import {Input} from "react-bootstrap";

import edit from "app/components/higherOrder/edit";
import hasPermission from "app/components/higherOrder/hasPermission";


class Form extends React.Component {
    render() {
        const {changeSet, handleChange, handleSubmit} = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Input
                    bsStyle={changeSet._errors.device_id ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.device_id}
                    label="Device ID"
                    name="device_id"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.device_id}
                    help={changeSet._errors.device_id}
                />
                <Input
                    bsStyle={changeSet._errors.sn ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.sn}
                    label="SN"
                    name="sn"
                    type="text"
                    onChange={handleChange}
                    value={changeSet.sn}
                />
                <Input
                    autoComplete="off"
                    bsStyle={changeSet._errors.device_alias ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.device_alias}
                    label="Device Alias"
                    name="device_alias"
                    onChange={handleChange}
                    type="text"
                    value={changeSet.device_alias}
                />
                <Input
                    autoComplete="off"
                    bsStyle={changeSet._errors.device_model ? "error" : null}
                    hasFeedback
                    help={changeSet._errors.device_model}
                    label="Device Model"
                    name="device_model"
                    onChange={handleChange}
                    type="text"
                    value={changeSet.device_model}
                />
                <button type="submit" className="hidden"/>
            </form>
        );
    }
}

export default hasPermission(edit(Form), "device_physical.change_phydevice");
