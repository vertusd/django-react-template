import {combineReducers} from "redux";

import adminlte from "adminlte/reducers";
import alerts from "app/reducers/alerts";
import physical_device from "app/physical_device/reducers";


export default combineReducers({
    adminlte,
    alerts,
    physical_device
});
