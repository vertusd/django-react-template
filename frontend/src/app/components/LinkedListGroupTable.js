import React from "react";
import {Link} from "react-router";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';


class LinkedListGroupTable extends React.Component {

    handleBtnClick = () => {
        if (order === 'desc') {
          this.refs.table.handleSort('asc', 'name');
          order = 'asc';
        } else {
          this.refs.table.handleSort('desc', 'name');
          order = 'desc';
        }
      }

     onSelectAll = (isSelected) => {
        if (isSelected) {
          return products.map(row => row.id);
        } else {
          return [];
        }
      }

    getSelectedRows() {
      console.log(Object.keys(this.props.models))
      return Object.keys(this.props.models)
    }
    
    onAfterDeleteRow = (rowKeys, rows) => {
      const {actions} = this.props;
      rows.forEach(model => {
        actions.deleteModel({model});
      });
    }
    
    render() {
        const {collection} = this.props;
        const selectRowProp = {
            mode: 'checkbox',
            clickToSelect: true,
            onSelectAll: this.onSelectAll,
            //selected: this.getSelectedRows(),
            search: true,
            multiColumnSearch: true,
          };
        const options = {
          afterDeleteRow: this.onAfterDeleteRow  // A hook for after droping rows.
        };
        return (

            <div>
            <BootstrapTable ref='table' data={ collection.models.toArray() } deleteRow={ true } selectRow={ selectRowProp }  options={ options} pagination>
                <TableHeaderColumn dataField='id' isKey={ true } dataSort={ true }>ID</TableHeaderColumn>
                <TableHeaderColumn dataField='device_model' dataSort={ true }>Device ID</TableHeaderColumn>
                <TableHeaderColumn dataField='sn' dataSort={ true }> SN</TableHeaderColumn>
            </BootstrapTable>
            </div>


        );
    }
}

export default LinkedListGroupTable;

/*
class LinkedListGroupTable extends React.Component {
    render() {
        const {collection} = this.props;

        return (
            <table className="table table-bordered table-striped dataTable">
                <thead>
                    <tr>
                        <td><strong>Device ID</strong></td>
                        <td><strong>SN</strong></td>
                        <td><strong>Device Alias</strong></td>
                        <td><strong>Device Model</strong></td>
                        <td><strong>Rack</strong></td>
                        <td><strong>Position</strong></td>
                        <td><strong>U Position</strong></td>
                        <td><strong>Device Height</strong></td>
                        <td><strong>Vendor</strong></td>
                        <td><strong>Logical Area</strong></td>
                        <td><strong>Note</strong></td>
                        <td><strong>Region</strong></td>
                        <td><strong>IP Address</strong></td>
                        <td><strong>Cloud Product</strong></td>
                        <td><strong>Intranet Switch Port</strong></td>
                        <td><strong>Management Switch Port</strong></td>
                    </tr>
                </thead>

                <tbody>
                {collection.models.toList().map((model, key) =>
                    <tr
                        key={key}
                        to={model.appUrl()}
                    >
                        <td>{model.toDevice_id()}</td>
                        <td>{model.toSN()}</td>
                        <td>{model.toDevice_alias()}</td>
                        <td>{model.toDevice_model()}</td>
                        <td>{model.toRack()}</td>
                        <td>{model.toPosition()}</td>
                        <td>{model.toU_position()}</td>
                        <td>{model.toDevice_height()}</td>
                        <td>{model.toVendor()}</td>
                        <td>{model.toLogical_area()}</td>
                        <td>{model.toNote()}</td>
                        <td>{model.toRegion()}</td>
                        <td>{model.toIP_addr()}</td>
                        <td>{model.toCloud_product()}</td>
                        <td>{model.toIntranet_switchport()}</td>
                        <td>{model.toManagement_switchport()}</td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }
}

export default LinkedListGroupTable;
*/



/*
const columns = [{
  dataField: 'id',
  text: 'Product ID',
  sort: true
}, {
  dataField: 'name',
  text: 'Product Name',
  sort: true
}, {
  dataField: 'price',
  text: 'Product Price',
  sort: true
}];

const defaultSorted = [{
  dataField: 'name',
  order: 'desc'
}];

<BootstrapTable
    bootstrap4
    keyField="id"
    data={ products }
    columns={ columns }
    defaultSorted={ defaultSorted }
/>
*/
