# Generated by Django 2.2.5 on 2019-09-19 06:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('physical_device', '0002_auto_20190919_1303'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='phydevice',
            name='cloud_product',
        ),
        migrations.RemoveField(
            model_name='phydevice',
            name='float_ip',
        ),
        migrations.RemoveField(
            model_name='phydevice',
            name='hardware_maintenance_sla',
        ),
        migrations.RemoveField(
            model_name='phydevice',
            name='internet_switchport',
        ),
        migrations.RemoveField(
            model_name='phydevice',
            name='intranet_switchport',
        ),
        migrations.RemoveField(
            model_name='phydevice',
            name='ip_addr',
        ),
        migrations.RemoveField(
            model_name='phydevice',
            name='management_switchport',
        ),
        migrations.RemoveField(
            model_name='phydevice',
            name='product_manager',
        ),
    ]
