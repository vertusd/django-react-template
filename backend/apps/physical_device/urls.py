from rest_framework.routers import DefaultRouter

from .views import PhyDeviceViewSet

router = DefaultRouter()
router.register(r'physical_device', PhyDeviceViewSet)
urlpatterns = router.urls
