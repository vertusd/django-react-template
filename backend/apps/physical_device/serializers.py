from rest_framework import serializers
from .models import PhyDevice


class PhyDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhyDevice
        fields = (
            'id', 'device_id', 'sn', 'device_alias', 'device_model', 'rack', 'position',
            'u_position', 'device_height', 'vendor', 'logical_area', 'note', 'region',
            'ip_addr', 'cloud_product', 'intranet_switchport', 'management_switchport'
        )


"""
from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'first_name', 'last_name', 'email', 'last_login',
            'is_active', 'date_joined', 'last_updated'
        )
"""


