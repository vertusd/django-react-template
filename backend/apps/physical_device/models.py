from __future__ import unicode_literals

from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin,
                                        BaseUserManager)
from django.db import models
from django.utils import timezone


class PhyDevice(models.Model):

    device_id = models.CharField(max_length=100, verbose_name='固资编号')
    sn = models.CharField(max_length=100, verbose_name='SN')
    device_alias = models.CharField(max_length=100, verbose_name='设备别名', blank=True)
    device_model = models.CharField(max_length=100, verbose_name='设备型号', blank=True)
    rack = models.CharField(max_length=100, verbose_name='机架', blank=True)
    position = models.CharField(max_length=100, verbose_name='机位', blank=True)
    u_position = models.CharField(max_length=100, verbose_name='u位', blank=True)
    device_height = models.CharField(max_length=100, verbose_name='设备高度', blank=True)
    vendor = models.CharField(max_length=100, verbose_name='厂商', blank=True)
    logical_area = models.CharField(max_length=100, verbose_name='逻辑区域', blank=True)
    note = models.CharField(max_length=100, verbose_name='硬件备注', blank=True)
    region = models.CharField(max_length=100, verbose_name='地域', blank=True)
    ip_addr = models.CharField(max_length=100, verbose_name='内网IP地址', blank=True)
    cloud_product = models.CharField(max_length=100, verbose_name='所属云产品', blank=True)
    intranet_switchport = models.CharField(max_length=100, verbose_name='内网交换机端口', blank=True)
    management_switchport = models.CharField(max_length=100, verbose_name='管理网交换机端口', blank=True)

    class Meta:
        verbose_name = '物理设备资产表'
        verbose_name_plural = verbose_name
        db_table = 'physical_device'
