from __future__ import unicode_literals

from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin,
                                        BaseUserManager)
from django.db import models
from django.utils import timezone


class NetDevice(models.Model):
    dev_name = models.CharField(max_length=50, verbose_name='设备名称', blank=True)
    asset_num = models.CharField(max_length=50, verbose_name='固资编号', blank=True)
    sn = models.CharField(max_length=50, verbose_name='SN', blank=True)
    dev_type = models.CharField(max_length=50, verbose_name='设备类型', blank=True)
    application = models.CharField(max_length=50, verbose_name='设备用途', blank=True)
    brand = models.CharField(max_length=50, verbose_name='设备厂家', blank=True)
    dev_model = models.CharField(max_length=50, verbose_name='设备型号', blank=True)
    idc = models.CharField(max_length=100, verbose_name='IDC', blank=True)
    ip_addr = models.CharField(max_length=100, verbose_name='管理IP', blank=True)
    region = models.CharField(max_length=50, verbose_name='地域', blank=True)
    comment = models.CharField(max_length=300, verbose_name='备注', blank=True)
    #modify_time = models.DateTimeField(default=datetime.now, verbose_name='修改时间')


    class Meta:
        verbose_name = '网络设备资产表'
        verbose_name_plural = verbose_name
