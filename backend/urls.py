from django.conf.urls import url, include
#from django.contrib.auth.views import login, logout_then_login
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib import admin

from .views import app, index

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('physical_device.urls')),
    url(r'^app/', app, name='app'),
    url('^auth/login/$', LoginView.as_view(), name='login'),
    url('^auth/logout/$', LogoutView.as_view(), name='logout'),
    url('^$', index, name='index'),
]
